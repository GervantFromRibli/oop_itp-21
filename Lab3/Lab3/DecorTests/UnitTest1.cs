﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Lab3Stream;
using System.Collections.Generic;
using System.IO;

namespace DecorTests
{
    [TestClass]
    public class DecoratorTests
    {
        [TestMethod]
        public void TextWrite1()
        {
            string a = "Hello world";
            LabStream stream = new LabStream("d:\\text.txt", Stream.Null);
            List<byte> data = new List<byte>();
            foreach (var elem in a)
            {
                data.Add(Convert.ToByte(elem));
            }
            stream.Write(data.ToArray(), 0, a.Length);
            a = "123456789101112131415161718192021222324252627282930";
            data = new List<byte>();
            foreach (var elem in a)
            {
                data.Add(Convert.ToByte(elem));
            }
            stream.Write(data.ToArray(), 0, a.Length);
            a = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
            data = new List<byte>();
            foreach (var elem in a)
            {
                data.Add(Convert.ToByte(elem));
            }
            stream.Write(data.ToArray(), 0, a.Length);
            StreamReader reader = new StreamReader("d:\\text.txt");
            List<string> check = new List<string>();
            while (reader.Peek() >= 0)
            {
                a = reader.ReadLine();
                check.Add(a);
            }
            Assert.AreEqual(check[0].Length, 102);
        }
        [TestMethod]
        public void TextWrite2()
        {
            string a = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaHello world123456789101112131415161718192021222324252627282930";
            LabStream stream = new LabStream("d:\\text.txt", Stream.Null);
            List<byte> data = new List<byte>();
            foreach (var elem in a)
            {
                data.Add(Convert.ToByte(elem));
            }
            stream.Write(data.ToArray(), 0, a.Length);
            StreamReader reader = new StreamReader("d:\\text.txt");
            List<string> check = new List<string>();
            while (reader.Peek() >= 0)
            {
                a = reader.ReadLine();
                check.Add(a);
            }
            Assert.AreEqual(check[0], 102);
        }
        [TestMethod]
        public void TextWrite3()
        {
            string a = "Hello world";
            LabStream stream = new LabStream("d:\\text.txt", Stream.Null);
            List<byte> data = new List<byte>();
            foreach (var elem in a)
            {
                data.Add(Convert.ToByte(elem));
            }
            stream.Write(data.ToArray(), 0, a.Length);
            a = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
            data = new List<byte>();
            foreach (var elem in a)
            {
                data.Add(Convert.ToByte(elem));
            }
            stream.Write(data.ToArray(), 0, a.Length);
            StreamReader reader = new StreamReader("d:\\text.txt");
            List<string> check = new List<string>();
            while (reader.Peek() >= 0)
            {
                a = reader.ReadLine();
                check.Add(a);
            }
            Assert.AreEqual(check.Count, 0);
        }
    }
}
