﻿using System;
using System.Data.Linq.Mapping;

namespace DBElements
{
    /// <summary>
    /// Class that represent the "Train" type of data
    /// </summary>
    [Table(Name = "Train")]
    public class Train
    {
        private int id;
        /// <summary>
        /// Property to check the incoming id
        /// </summary>
        [Column(Name = "TrainNumber", CanBeNull = false, IsDbGenerated = false, Storage = "id", IsPrimaryKey = true)]
        public int Id
        {
            get => id;
            set
            {
                if (value.ToString().Length != 3)
                {
                    throw new Exception("Wrong Id");
                }
                else
                {
                    id = value;
                }
            }
        }
        /// <summary>
        /// Place of departure
        /// </summary>
        [Column(Name = "DepartPlace", CanBeNull = false, IsDbGenerated = false)]
        public int DepartPlace;
        /// <summary>
        /// Place of arriving
        /// </summary>
        [Column(Name = "ArrivePlace", CanBeNull = false, IsDbGenerated = false)]
        public int ArrivePlace;
        private DateTime departTime;
        /// <summary>
        /// Property to check the time of departure
        /// </summary>
        [Column(Name = "DepartTime", CanBeNull = false, IsDbGenerated = false, Storage = "departTime")]
        public DateTime DepartTime
        {
            get => departTime;
            set
            {
                if (value.Hour > 0 && value.Hour < 24 && value.Minute > 0 && value.Minute < 60)
                {
                    departTime = value;
                }
                else
                {
                    throw new Exception("Wrong time");
                }
            }
        }
        private DateTime pathTime;
        /// <summary>
        /// Property to check the path time
        /// </summary>
        [Column(Name = "PathTime", CanBeNull = false, IsDbGenerated = false, Storage = "pathTime")]
        public DateTime PathTime
        {
            get => pathTime;
            set
            {
                if (value.Hour > 0 && value.Minute > 0 && value.Minute < 60)
                {
                    pathTime = value;
                }
                else
                {
                    throw new Exception("Wrong time");
                }
            }
        }
        /// <summary>
        /// Type of train
        /// </summary>
        [Column(Name = "Type", CanBeNull = false, IsDbGenerated = false)]
        public int type;
        /// <summary>
        /// Constructor to create the "train" object
        /// </summary>
        /// <param name="id"> Number of train </param>
        /// <param name="departPlace"> Place of departure </param>
        /// <param name="arrivePlace"> Place of arriving </param>
        /// <param name="departTime"> Time of departure </param>
        /// <param name="pathTime"> Path time </param>
        /// <param name="type"> Type of train </param>
        public Train(int id, int departPlace, int arrivePlace, DateTime departTime, DateTime pathTime, int type)
        {
            Id = id;
            DepartPlace = departPlace;
            ArrivePlace = arrivePlace;
            DepartTime = departTime;
            PathTime = pathTime;
            this.type = type;
        }
        /// <summary>
        /// Constructor to create the "train" object
        /// </summary>
        public Train() { }
    }
}
