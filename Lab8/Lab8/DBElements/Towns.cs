﻿using System;
using System.Data.Linq.Mapping;

namespace DBElements
{
    /// <summary>
    /// Class for "Towns" type of data
    /// </summary>
    [Table(Name = "Towns")]
    public class Towns
    {
        private int id;
        /// <summary>
        /// Property to check incoming id
        /// </summary>
        [Column(Name = "TownId", CanBeNull = false, IsDbGenerated = false, Storage = "id", IsPrimaryKey = true)]
        public int Id
        {
            get => id;
            set
            {
                if (value.ToString().Length != 2)
                {
                    throw new Exception("Wrong Id");
                }
                else
                {
                    id = value;
                }
            }
        }
        /// <summary>
        /// Name of town
        /// </summary>
        [Column(Name = "Name", CanBeNull = false, IsDbGenerated = false)]
        public string Name { get; set; }
        /// <summary>
        /// Constructor for creating "Towns" object
        /// </summary>
        /// <param name="id"> Id of town </param>
        /// <param name="name"> Name of town </param>
        public Towns(int id, string name)
        {
            Id = id;
            Name = name;
        }
        /// <summary>
        /// Constructor for creating "Towns" object
        /// </summary>
        public Towns() { }
    }
}
