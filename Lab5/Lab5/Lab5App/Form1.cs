﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Lab5Calc;
using Lab5File;

namespace Lab5App
{
    public partial class Form1 : Form
    {
        private event GetPath PathValue;
        private delegate void GetPath();
        Label infoLabel = new Label()
        {
            Text = "Данные",
            Location = new Point(250, 30)
        };
        Label resultLabel = new Label()
        {
            Text = "Результат",
            Location = new Point(700, 30)
        };
        TextBox infoBox = new TextBox()
        {
            Size = new Size(300, 300),
            Location = new Point(120, 60),
            Multiline = true,
            ReadOnly = true
        };
        TextBox resultBox = new TextBox()
        {
            Size = new Size(300, 300),
            Location = new Point(580, 60),
            Multiline = true,
            ReadOnly = true
        };
        TextBox filePath = new TextBox()
        {
            Location = new Point(200, 500),
            Size = new Size(200, 20)
        };
        Button getPath = new Button()
        {
            Location = new Point(430, 500),
            Text = "Файл..."
        };
        readonly Calculator calculator;
        public Form1()
        {
            InitializeComponent();
            Width = 1000;
            Height = 600;
            Controls.Add(infoLabel);
            Controls.Add(resultLabel);
            Controls.Add(infoBox);
            Controls.Add(resultBox);
            Controls.Add(filePath);
            Controls.Add(getPath);
            PathValue += PathGet;
            getPath.Click += GetPath_Click;
            calculator = new Calculator();
        }
        private void PathGet()
        {
            OpenFileDialog openFile = new OpenFileDialog();
            openFile.Filter = "Text files(*.txt)|*.txt";
            if (openFile.ShowDialog() == DialogResult.OK)
            {
                filePath.Text = openFile.FileName;
                GetResult(openFile.FileName);
            }
            GetInfo(openFile.FileName);
        }
        private void GetPath_Click(object sender, EventArgs e)
        {
            PathValue.Invoke();
        }
        private void GetInfo(string path)
        {
            infoBox.Text = "";
            foreach (var elem in FileReader.Read(path))
            {
                infoBox.Text += elem + Environment.NewLine;
            }
        }
        private void GetResult(string path)
        {
            resultBox.Text = "";
            foreach (var elem in calculator.GetResult(FileReader.Read(path)))
            {
                resultBox.Text += elem.ToString() + Environment.NewLine;
            }
        }
    }
}
