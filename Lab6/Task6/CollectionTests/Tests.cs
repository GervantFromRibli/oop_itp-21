﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Collection;
using TrainClass;

namespace CollectionTests
{
    [TestClass]
    public class BufferInt
    {
        [TestMethod]
        public void BufferCreate()
        {
            CycleBuffer<int> vs = new CycleBuffer<int>(20);
            Assert.AreEqual(0, vs[19]);
        }
        [TestMethod]
        public void BufferCreate2()
        {
            CycleBuffer<int> vs = new CycleBuffer<int>(20, 4);
            Assert.AreEqual(20, vs[0]);
        }
        [TestMethod]
        public void BufferCreate3()
        {
            CycleBuffer<int> vs = new CycleBuffer<int>(new int[2] { 4, 5}, 4);
            Assert.AreEqual(5, vs[1]);
        }
        [TestMethod]
        public void BufferCount1()
        {
            CycleBuffer<int> vs = new CycleBuffer<int>(new int[2] { 4, 5 }, 4);
            Assert.AreEqual(2, vs.Count);
        }
        [TestMethod]
        public void BufferCount2()
        {
            CycleBuffer<int> vs = new CycleBuffer<int>(new int[2] { 4, 5 }, 2);
            vs.Add(7);
            Assert.AreEqual(2, vs.Count);
        }
        [TestMethod]
        public void BufferAdd()
        {
            CycleBuffer<int> vs = new CycleBuffer<int>(new int[2] { 4, 5 }, 2);
            vs.Add(7);
            Assert.AreEqual(7, vs[0]);
        }
        [TestMethod]
        public void BufferAdd2()
        {
            CycleBuffer<int> vs = new CycleBuffer<int>(new int[2] { 4, 5 }, 2);
            vs.Add(new int[] { 7, 4});
            Assert.AreEqual(4, vs[1]);
        }
        [TestMethod]
        public void BufferClear()
        {
            CycleBuffer<int> vs = new CycleBuffer<int>(new int[10] { 4, 5, 7, 5, 1, 0, 9, 2, 7, 8 }, 10);
            vs.Clear();
            Assert.AreEqual(0, vs[9]);
        }
        [TestMethod]
        public void BufferContains()
        {
            CycleBuffer<int> vs = new CycleBuffer<int>(new int[10] { 4, 5, 7, 5, 1, 0, 9, 2, 7, 8 }, 10);
            Assert.AreEqual(true, vs.Contains(8));
        }
        [TestMethod]
        public void BufferContains2()
        {
            CycleBuffer<int> vs = new CycleBuffer<int>(new int[10] { 4, 5, 7, 5, 1, 0, 9, 2, 7, 8 }, 10);
            Assert.AreEqual(false, vs.Contains(3));
        }
    }

    [TestClass]
    public class BufferString
    {
        [TestMethod]
        public void BufferCreate()
        {
            CycleBuffer<string> vs = new CycleBuffer<string>(20);
            Assert.AreEqual(null, vs[19]);
        }
        [TestMethod]
        public void BufferCreate2()
        {
            CycleBuffer<string> vs = new CycleBuffer<string>("as", 4);
            Assert.AreEqual("as", vs[0]);
        }
        [TestMethod]
        public void BufferCreate3()
        {
            CycleBuffer<string> vs = new CycleBuffer<string>(new string[2] { "as", "bs" }, 4);
            Assert.AreEqual("bs", vs[1]);
        }
        [TestMethod]
        public void BufferCount1()
        {
            CycleBuffer<string> vs = new CycleBuffer<string>(new string[2] { "as", "bs" }, 4);
            Assert.AreEqual(2, vs.Count);
        }
        [TestMethod]
        public void BufferCount2()
        {
            CycleBuffer<string> vs = new CycleBuffer<string>(new string[2] { "as", "bs" }, 2);
            vs.Add("ks");
            Assert.AreEqual(2, vs.Count);
        }
        [TestMethod]
        public void BufferAdd()
        {
            CycleBuffer<string> vs = new CycleBuffer<string>(new string[2] { "as", "bs" }, 2);
            vs.Add("ks");
            Assert.AreEqual("ks", vs[0]);
        }
        [TestMethod]
        public void BufferAdd2()
        {
            CycleBuffer<string> vs = new CycleBuffer<string>(new string[2] { "as", "bs" }, 2);
            vs.Add(new string[] { "ks", "tank" });
            Assert.AreEqual("tank", vs[1]);
        }
        [TestMethod]
        public void BufferClear()
        {
            CycleBuffer<string> vs = new CycleBuffer<string>(new string[10] { "as", "bs", "cs", "ds", "es", "fs", "gs", "hs", "is", "js" }, 10);
            vs.Clear();
            Assert.AreEqual(null, vs[9]);
        }
        [TestMethod]
        public void BufferContains()
        {
            CycleBuffer<string> vs = new CycleBuffer<string>(new string[10] { "as", "bs", "cs", "ds", "es", "fs", "gs", "hs", "is", "js" }, 10);
            Assert.AreEqual(true, vs.Contains("as"));
        }
        [TestMethod]
        public void BufferContains2()
        {
            CycleBuffer<string> vs = new CycleBuffer<string>(new string[10] { "as", "bs", "cs", "ds", "es", "fs", "gs", "hs", "is", "js" }, 10);
            Assert.AreEqual(false, vs.Contains("qs"));
        }
    }

    [TestClass]
    public class BufferTrain
    {
        [TestMethod]
        public void BufferCreate()
        {
            CycleBuffer<Train> vs = new CycleBuffer<Train>(20);
            Assert.AreEqual(null, vs[19]);
        }
        [TestMethod]
        public void BufferCreate2()
        {
            CycleBuffer<Train> vs = new CycleBuffer<Train>(new Train(545, "dawda", "awawqq", 15, 45, 20, 35, Types.Пассажирский), 4);
            Assert.AreEqual(545, vs[0].Number);
        }
        [TestMethod]
        public void BufferCreate3()
        {
            CycleBuffer<Train> vs = new CycleBuffer<Train>(new Train[2] { new Train(545, "dawda", "awawqq", 15, 45, 20, 35, Types.Пассажирский), new Train(565, "dawda", "awawqq", 15, 40, 20, 35, Types.Скорый) }, 4);
            Assert.AreEqual(565, vs[1].Number);
        }
        [TestMethod]
        public void BufferCount1()
        {
            CycleBuffer<Train> vs = new CycleBuffer<Train>(new Train[2] { new Train(545, "dawda", "awawqq", 15, 45, 20, 35, Types.Пассажирский), new Train(565, "dawda", "awawqq", 15, 40, 20, 35, Types.Скорый) }, 4);
            Assert.AreEqual(2, vs.Count);
        }
        [TestMethod]
        public void BufferCount2()
        {
            CycleBuffer<Train> vs = new CycleBuffer<Train>(new Train[2] { new Train(545, "dawda", "awawqq", 15, 45, 20, 35, Types.Пассажирский), new Train(565, "dawda", "awawqq", 15, 40, 20, 35, Types.Скорый) }, 2);
            vs.Add(new Train(545, "dawda", "awawqq", 15, 45, 20, 35, Types.Пассажирский));
            Assert.AreEqual(2, vs.Count);
        }
        [TestMethod]
        public void BufferAdd()
        {
            CycleBuffer<Train> vs = new CycleBuffer<Train>(new Train[2] { new Train(545, "dawda", "awawqq", 15, 45, 20, 35, Types.Пассажирский), new Train(565, "dawda", "awawqq", 15, 40, 20, 35, Types.Скорый) }, 2);
            vs.Add(new Train(570, "wadqq", "dwaq", 14, 50, 60, 40, Types.Экспресс));
            Assert.AreEqual(570, vs[0].Number);
        }
        [TestMethod]
        public void BufferAdd2()
        {
            CycleBuffer<Train> vs = new CycleBuffer<Train>(new Train[2] { new Train(545, "dawda", "awawqq", 15, 45, 20, 35, Types.Пассажирский), new Train(565, "dawda", "awawqq", 15, 40, 20, 35, Types.Скорый) }, 2);
            vs.Add(new Train[2] { new Train(545, "dawda", "awawqq", 15, 45, 20, 35, Types.Пассажирский), new Train(615, "dawda", "awawqq", 15, 40, 20, 35, Types.Пассажирский) });
            Assert.AreEqual(615, vs[1].Number);
        }
        [TestMethod]
        public void BufferClear()
        {
            CycleBuffer<Train> vs = new CycleBuffer<Train>(new Train[2] { new Train(545, "dawda", "awawqq", 15, 45, 20, 35, Types.Пассажирский), new Train(565, "dawda", "awawqq", 15, 40, 20, 35, Types.Скорый) }, 2);
            vs.Clear();
            Assert.AreEqual(null, vs[9]);
        }
        [TestMethod]
        public void BufferContains()
        {
            CycleBuffer<Train> vs = new CycleBuffer<Train>(new Train[2] { new Train(545, "dawda", "awawqq", 15, 45, 20, 35, Types.Пассажирский), new Train(565, "dawda", "awawqq", 15, 40, 20, 35, Types.Скорый) }, 2);
            Assert.AreEqual(true, vs.Contains(new Train(545, "dawda", "awawqq", 15, 45, 20, 35, Types.Пассажирский)));
        }
        [TestMethod]
        public void BufferContains2()
        {
            CycleBuffer<Train> vs = new CycleBuffer<Train>(new Train[2] { new Train(545, "dawda", "awawqq", 15, 45, 20, 35, Types.Пассажирский), new Train(565, "dawda", "awawqq", 15, 40, 20, 35, Types.Скорый) }, 2);
            Assert.AreEqual(false, vs.Contains(new Train(615, "dawda", "awawqq", 15, 45, 20, 35, Types.Пассажирский)));
        }
    }
}
