﻿using System;
using System.Drawing;

namespace Lab4Form
{
    public static class DrawClass
    {
        public static Brush color = Brushes.Black;
        public static Bitmap Draw(Bitmap bitmap, Point startPoint, Point endPoint)
        {
            Graphics graphics = Graphics.FromImage(bitmap);
            graphics.FillRectangle(color, startPoint.X < endPoint.X ? startPoint.X : endPoint.X, startPoint.Y < endPoint.Y ? startPoint.Y : endPoint.Y, Math.Abs(endPoint.X - startPoint.X), Math.Abs(endPoint.Y - startPoint.Y));
            return bitmap;
        }
        public static void ChangeBrush()
        {
            color = color == Brushes.Black ? Brushes.AntiqueWhite : Brushes.Black;
        }
    }
}
