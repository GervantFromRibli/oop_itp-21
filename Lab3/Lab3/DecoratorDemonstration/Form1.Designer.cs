﻿namespace DecoratorDemonstration
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.text = new System.Windows.Forms.TextBox();
            this.StreamButton = new System.Windows.Forms.Button();
            this.LabStreamButton = new System.Windows.Forms.Button();
            this.ShowBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("News706 BT", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(279, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(240, 22);
            this.label1.TabIndex = 0;
            this.label1.Text = "Демонстрация декоратора";
            // 
            // text
            // 
            this.text.Location = new System.Drawing.Point(168, 331);
            this.text.Name = "text";
            this.text.Size = new System.Drawing.Size(441, 20);
            this.text.TabIndex = 2;
            // 
            // StreamButton
            // 
            this.StreamButton.Location = new System.Drawing.Point(127, 385);
            this.StreamButton.Name = "StreamButton";
            this.StreamButton.Size = new System.Drawing.Size(170, 23);
            this.StreamButton.TabIndex = 3;
            this.StreamButton.Text = "Добавления через Stream";
            this.StreamButton.UseVisualStyleBackColor = true;
            this.StreamButton.Click += new System.EventHandler(this.StreamClick);
            // 
            // LabStreamButton
            // 
            this.LabStreamButton.Location = new System.Drawing.Point(413, 385);
            this.LabStreamButton.Name = "LabStreamButton";
            this.LabStreamButton.Size = new System.Drawing.Size(190, 23);
            this.LabStreamButton.TabIndex = 4;
            this.LabStreamButton.Text = "Добавление через декоратор";
            this.LabStreamButton.UseVisualStyleBackColor = true;
            this.LabStreamButton.Click += new System.EventHandler(this.LabStreamButton_Click);
            // 
            // ShowBox
            // 
            this.ShowBox.Location = new System.Drawing.Point(168, 68);
            this.ShowBox.Multiline = true;
            this.ShowBox.Name = "ShowBox";
            this.ShowBox.Size = new System.Drawing.Size(441, 257);
            this.ShowBox.TabIndex = 5;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.ShowBox);
            this.Controls.Add(this.LabStreamButton);
            this.Controls.Add(this.StreamButton);
            this.Controls.Add(this.text);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox text;
        private System.Windows.Forms.Button StreamButton;
        private System.Windows.Forms.Button LabStreamButton;
        private System.Windows.Forms.TextBox ShowBox;
    }
}

