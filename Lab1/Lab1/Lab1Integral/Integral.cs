﻿using System;

namespace Lab1Integral
{
    public class Integral
    {
        private float b;
        public float A { get; set; }
        public float B
        {
            get => b;
            set
            {
                if (value <= A)
                {
                    throw new Exception("Неправильный интервал");
                }
                else
                {
                    b = value;
                }
            }
        }
        public Func<float, float> integral;
        public float GetIntegralByTrap()
        {
            if (integral == null)
            {
                throw new Exception("Нет функции");
            }
            float h = (B - A) / 50;
            float promezhResult = 0;
            for (float x = A + h; x < B - h; x += h)
            {
                promezhResult += integral(x) * h;
            }
            return integral(A) * h / 2 + promezhResult + integral(B) * h / 2; 
        }
        public Integral(float A, float B, Func<float, float> func)
        {
            this.A = A;
            this.B = B;
            integral = func;
        }
    }
}
