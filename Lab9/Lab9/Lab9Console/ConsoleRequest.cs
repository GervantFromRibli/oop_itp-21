﻿using Lab9Linq;
using System;
using System.Linq;

namespace Lab9Console
{
    class ConsoleRequest
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Происходит считывание...");
            var Words = Request.TaskRequest();

            foreach (var group in Words)
            {
                if (group.Key != null)
                {
                    Console.WriteLine("Слово: " + group.Key + " встречается в тексте " + group.Count() + " раз");
                }
            }
            Console.ReadKey();
        }
    }
}
