﻿using System;
using System.Collections.Generic;
using System.Linq;
using DBElements;

namespace Commands
{
    public static class Comm
    {
        public static string[] GetNames(List<Towns> towns)
        {
            var names = from town in towns
                        select town.Name;
            return names.ToArray();
        }
        public static string[] GetNames(List<Types> types)
        {
            var type = from elem in types
                       select elem.Name;
            return type.ToArray();
        }
        public static int GetCertainId(List<Towns> towns, string name)
        {
            var elem = from town in towns
                       where town.Name == name
                       select town.Id;
            return elem.First();
        }
        public static int GetCertainId(List<Types> types, string name)
        {
            var elem = from type_check in types
                       where type_check.Name == name
                       select type_check.Id;
            return elem.First();
        }
        public static string GetCertainName(List<Towns> towns, int id)
        {
            var elem = from town in towns
                       where town.Id == id
                       select town.Name;
            return elem.First();
        }
        public static string GetCertainName(List<Types> types, int id)
        {
            var elem = from type in types
                       where type.Id == id
                       select type.Name;
            return elem.First();
        }
        public static TimeClass GetTime(string time)
        {
            var timeElems = time.Trim().Split(':').ToArray();
            return new TimeClass(Convert.ToInt32(timeElems[0]), Convert.ToInt32(timeElems[1]));
        }
    }
}
