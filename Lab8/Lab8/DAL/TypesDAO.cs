﻿using DBElements;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;

namespace DAL
{
    public class TypesDAO : IDAO<Types>
    {
        public string connectionString = @"Data Source=.\SQLEXPRESS;Initial Catalog=Trains; Integrated Security=true";
        public void Create(Types obj)
        {
            DataContext db = new DataContext(connectionString);
            db.GetTable<Types>().InsertOnSubmit(obj);
            db.SubmitChanges();
        }
        public void Delete(int id)
        {
            DataContext db = new DataContext(connectionString);
            var elem = db.GetTable<Types>().Select(type => type).Where(type => type.Id == id).SingleOrDefault();
            db.GetTable<Types>().DeleteOnSubmit(elem);
            db.SubmitChanges();
        }
        public List<Types> GetAll()
        {
            DataContext db = new DataContext(connectionString);
            Table<Types> elems = db.GetTable<Types>();
            List<Types> objects_list = new List<Types>();
            foreach (var elem in elems.ToList())
            {
                elem.Name = elem.Name.Trim();
                objects_list.Add(elem);
            }
            return objects_list;
        }

        public Types Read(int id)
        {
            DataContext db = new DataContext(connectionString);
            var elem = db.GetTable<Types>().Select(type => type).Where(type => type.Id == id).SingleOrDefault();
            elem.Name = elem.Name.Trim();
            return elem;
        }

        public void Update(Types obj)
        {
            DataContext db = new DataContext(connectionString);
            var elem = db.GetTable<Types>().Select(type => type).Where(type => type.Id == obj.Id).SingleOrDefault();
            elem.Name = obj.Name;
            db.SubmitChanges();
        }
    }
}
