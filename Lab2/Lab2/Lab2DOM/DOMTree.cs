﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Xml;
using Trains;

namespace Lab2DOM
{
    public static class DOMTree
    {
        public static string path = "d:\\Trains.xml";
        public static void CreateDoc(List<Train> trains)
        {
            XmlDocument document = new XmlDocument();
            XmlElement document_root = document.CreateElement("Список_поездов");
            XmlElement types = document.CreateElement("Types");
            types.InnerText = "Типы поезда";
            XmlElement typefirst = document.CreateElement("Тип_поезда");
            typefirst.InnerText = Types.Пассажирский.ToString();
            XmlElement secondtype = document.CreateElement("Тип_поезда");
            secondtype.InnerText = Types.Скорый.ToString();
            XmlElement thirdtype = document.CreateElement("Тип_поезда");
            thirdtype.InnerText = Types.Экспресс.ToString();
            types.AppendChild(typefirst);
            types.AppendChild(secondtype);
            types.AppendChild(thirdtype);
            document_root.AppendChild(types);
            XmlElement traintitle = document.CreateElement("Title");
            traintitle.InnerText = "Поезда";
            foreach (var train in trains)
            {
                XmlElement trainElem = document.CreateElement("Поезд");
                trainElem.SetAttribute("Номер", train.Number.ToString());
                XmlElement departPlace = document.CreateElement("Пункт_отправления");
                departPlace.InnerText = train.DepartPlace;
                XmlElement destination = document.CreateElement("Пункт_назначения");
                destination.InnerText = train.Destination;
                XmlElement departTime = document.CreateElement("Время_отправления");
                departTime.InnerText = train.departTime.ToString();
                XmlElement pathTime = document.CreateElement("Продолжительность_в_пути");
                pathTime.InnerText = train.pathTime.ToString();
                XmlElement listOfStops = document.CreateElement("Список_остановок");
                listOfStops.InnerText = train.ListOfStop;
                XmlElement type = document.CreateElement("Тип");
                type.InnerText = train.type.ToString();
                trainElem.AppendChild(departPlace);
                trainElem.AppendChild(destination);
                trainElem.AppendChild(departTime);
                trainElem.AppendChild(pathTime);
                trainElem.AppendChild(listOfStops);
                trainElem.AppendChild(type);
                traintitle.AppendChild(trainElem);
            }
            document_root.AppendChild(traintitle);
            document.AppendChild(document_root);
            document.Save(path);
        }
        public static List<Train> ReadDoc()
        {
            List<Train> trains = new List<Train>();
            XmlDocument document = new XmlDocument();
            try
            {
                document.Load(path);
                XmlElement elements = document.DocumentElement;
                foreach (XmlNode node in elements.SelectSingleNode("descendant::Title"))
                {
                    if (node.ChildNodes.Count != 0 && node.Name != "Types" && node.InnerText != "Тип_поезда")
                    {
                        XmlNode attr = node.Attributes.GetNamedItem("Номер");
                        int number = Convert.ToInt32(attr.Value);
                        string departPlace = "";
                        string destPlace = "";
                        PathTime departTime = new PathTime();
                        PathTime pathTime = new PathTime();
                        string list = "";
                        Types type = Types.Пассажирский;
                        foreach (XmlNode xmlNode in node.ChildNodes)
                        {
                            switch (xmlNode.Name)
                            {
                                case "Пункт_отправления":
                                    departPlace = xmlNode.InnerText;
                                    break;
                                case "Пункт_назначения":
                                    destPlace = xmlNode.InnerText;
                                    break;
                                case "Время_отправления":
                                    string[] depTime = xmlNode.InnerText.Split(':');
                                    departTime.Hour = Convert.ToInt32(depTime[0]);
                                    departTime.Minutes = Convert.ToInt32(depTime[1]);
                                    break;
                                case "Продолжительность_в_пути":
                                    string[] timePath = xmlNode.InnerText.Split(':');
                                    pathTime.Hour = Convert.ToInt32(timePath[0]);
                                    pathTime.Minutes = Convert.ToInt32(timePath[1]);
                                    break;
                                case "Список_остановок":
                                    list = xmlNode.InnerText;
                                    break;
                                case "Тип":
                                    type = (Types)Enum.Parse(typeof(Types), xmlNode.InnerText);
                                    break;
                            }
                        }
                        trains.Add(new Train(number, destPlace, departPlace, departTime.Hour, departTime.Minutes, pathTime.Hour, pathTime.Minutes, type, list));
                    }
                    
                }
            }
            catch (FileNotFoundException)
            {
                MessageBox.Show("Нет файла");
            }
            return trains;
        }
    }
}
