﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    /// <summary>
    /// Interface for DAO
    /// </summary>
    /// <typeparam name="T"> Type of data </typeparam>
    public interface IDAO<T>
    {
        /// <summary>
        /// Method that returns the list of objects
        /// </summary>
        /// <returns> List of objects </returns>
        List<T> GetAll();
        /// <summary>
        /// Method that create an object in database
        /// </summary>
        /// <param name="obj"></param>
        void Create(T obj);
        /// <summary>
        /// Method that returns the object of T type with certain id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        T Read(int id);
        /// <summary>
        /// Method that update the data
        /// </summary>
        /// <param name="obj"></param>
        void Update(T obj);
        /// <summary>
        /// Method that delete data with certain id
        /// </summary>
        /// <param name="id"></param>
        void Delete(int id);
    }
}
