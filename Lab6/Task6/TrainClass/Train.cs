﻿using System;

namespace TrainClass
{
    public class Train
    {
        private int number;
        public int Number
        {
            get => number;
            set
            {
                if (value.ToString().Length != 3)
                {
                    throw new Exception("Неправильный номер");
                }
                else
                {
                    number = value;
                }
            }
        }
        public string Destination { get; set; }
        public string DepartPlace { get; set; }
        public PathTime departTime;
        public PathTime pathTime;
        public Types type;
        public Train(int Number, string Destination, string DepartPlace, int departHour, int departMinute, int pathHour, int pathMinute, Types type)
        {
            this.Number = Number;
            this.Destination = Destination;
            this.DepartPlace = DepartPlace;
            if (departHour > 23)
            {
                throw new Exception("Неправильный час отправления");
            }
            else
            {
                departTime = new PathTime(departHour, departMinute);
            }
            pathTime = new PathTime(pathHour, pathMinute);
            this.type = type;
        }
        public override string ToString()
        {
            return Number + "; " + DepartPlace + "; " + Destination + "; " + departTime.ToString() + "; " + pathTime.ToString() + "; " + type.ToString();
        }
        public override int GetHashCode()
        {
            return number.GetHashCode() + type.GetHashCode();
        }
    }
}
