﻿using DBElements;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class TypesDAO : IDAO<Types>
    {
        public string connectionString = @"Data Source=.\SQLEXPRESS;Initial Catalog=Trains; Integrated Security=true";
        public void Create(Types obj)
        {
            string command = $"INSERT INTO [Types] (TypeId, Name) VALUES (@Id, @Name)";
            SqlCommand cmd = new SqlCommand(command);
            cmd.Parameters.AddWithValue("@Id", obj.Id);
            cmd.Parameters.AddWithValue("@Name", obj.Name);
            SqlConnection connection = new SqlConnection(connectionString);
            connection.Open();
            cmd.Connection = connection;
            cmd.ExecuteNonQuery();
            connection.Close();
        }
        public void Delete(int id)
        {
            string command = $"Delete from [Types] where TypeId={id}";
            SqlCommand cmd = new SqlCommand(command);
            SqlConnection connection = new SqlConnection(connectionString);
            connection.Open();
            cmd.Connection = connection;
            cmd.ExecuteNonQuery();
            connection.Close();
        }
        public List<Types> GetAll()
        {
            string command = $"SELECT * FROM [Types]";
            SqlCommand cmd = new SqlCommand(command);
            SqlConnection connection = new SqlConnection(connectionString);
            connection.Open();
            cmd.Connection = connection;
            SqlDataReader dbreader = cmd.ExecuteReader();
            List<Types> objects_list = new List<Types>();
            while (dbreader.Read())
            {
                Types elem = new Types(dbreader.GetInt32(0), dbreader.GetString(1));
                objects_list.Add(elem);
            }
            connection.Close();
            return objects_list;
        }

        public Types Read(int id)
        {
            string command = $"SELECT * FROM [Types] WHERE [TypeId]={id}";
            SqlCommand cmd = new SqlCommand(command);
            SqlConnection connection = new SqlConnection(connectionString);
            connection.Open();
            Types elem = null;
            cmd.Connection = connection;
            SqlDataReader dbreader = cmd.ExecuteReader();
            if (dbreader.Read())
            {
                elem = new Types(dbreader.GetInt32(0), dbreader.GetString(1));
            }
            connection.Close();
            return elem;
        }

        public void Update(Types obj)
        {
            string command = $"UPDATE [Types] SET Name=@Name WHERE TypeId=@Id";
            SqlCommand cmd = new SqlCommand(command);
            cmd.Parameters.AddWithValue("@Id", obj.Id);
            cmd.Parameters.AddWithValue("@Name", obj.Name);
            SqlConnection connection = new SqlConnection(connectionString);
            connection.Open();
            cmd.Connection = connection;
            cmd.ExecuteNonQuery();
            connection.Close();
        }
    }
}
