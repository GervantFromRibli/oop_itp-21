﻿namespace Demonstration
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.AddButton = new System.Windows.Forms.Button();
            this.firstBox = new System.Windows.Forms.TextBox();
            this.firstLabel = new System.Windows.Forms.Label();
            this.DepartPlaceBox = new System.Windows.Forms.TextBox();
            this.DepartTimeBox = new System.Windows.Forms.TextBox();
            this.DestPlaceBox = new System.Windows.Forms.TextBox();
            this.PathTimeBox = new System.Windows.Forms.TextBox();
            this.typeLabel = new System.Windows.Forms.Label();
            this.typeBox = new System.Windows.Forms.ComboBox();
            this.DepartLabel = new System.Windows.Forms.Label();
            this.DestPlaceLabel = new System.Windows.Forms.Label();
            this.DepartTimeLabel = new System.Windows.Forms.Label();
            this.PathTimeLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.textBox1.Location = new System.Drawing.Point(114, 31);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(661, 366);
            this.textBox1.TabIndex = 0;
            // 
            // AddButton
            // 
            this.AddButton.Location = new System.Drawing.Point(668, 511);
            this.AddButton.Name = "AddButton";
            this.AddButton.Size = new System.Drawing.Size(134, 23);
            this.AddButton.TabIndex = 1;
            this.AddButton.Text = "Добавить";
            this.AddButton.UseVisualStyleBackColor = true;
            // 
            // firstBox
            // 
            this.firstBox.Location = new System.Drawing.Point(317, 425);
            this.firstBox.Name = "firstBox";
            this.firstBox.Size = new System.Drawing.Size(100, 20);
            this.firstBox.TabIndex = 2;
            // 
            // firstLabel
            // 
            this.firstLabel.AutoSize = true;
            this.firstLabel.Location = new System.Drawing.Point(240, 425);
            this.firstLabel.Name = "firstLabel";
            this.firstLabel.Size = new System.Drawing.Size(35, 13);
            this.firstLabel.TabIndex = 3;
            this.firstLabel.Text = "label1";
            // 
            // DepartPlaceBox
            // 
            this.DepartPlaceBox.Location = new System.Drawing.Point(174, 474);
            this.DepartPlaceBox.Name = "DepartPlaceBox";
            this.DepartPlaceBox.Size = new System.Drawing.Size(100, 20);
            this.DepartPlaceBox.TabIndex = 4;
            // 
            // DepartTimeBox
            // 
            this.DepartTimeBox.Location = new System.Drawing.Point(440, 474);
            this.DepartTimeBox.Name = "DepartTimeBox";
            this.DepartTimeBox.Size = new System.Drawing.Size(100, 20);
            this.DepartTimeBox.TabIndex = 5;
            // 
            // DestPlaceBox
            // 
            this.DestPlaceBox.Location = new System.Drawing.Point(174, 513);
            this.DestPlaceBox.Name = "DestPlaceBox";
            this.DestPlaceBox.Size = new System.Drawing.Size(100, 20);
            this.DestPlaceBox.TabIndex = 7;
            // 
            // PathTimeBox
            // 
            this.PathTimeBox.Location = new System.Drawing.Point(440, 513);
            this.PathTimeBox.Name = "PathTimeBox";
            this.PathTimeBox.Size = new System.Drawing.Size(100, 20);
            this.PathTimeBox.TabIndex = 8;
            // 
            // typeLabel
            // 
            this.typeLabel.AutoSize = true;
            this.typeLabel.Location = new System.Drawing.Point(504, 425);
            this.typeLabel.Name = "typeLabel";
            this.typeLabel.Size = new System.Drawing.Size(26, 13);
            this.typeLabel.TabIndex = 9;
            this.typeLabel.Text = "Тип";
            // 
            // typeBox
            // 
            this.typeBox.FormattingEnabled = true;
            this.typeBox.Location = new System.Drawing.Point(549, 425);
            this.typeBox.Name = "typeBox";
            this.typeBox.Size = new System.Drawing.Size(121, 21);
            this.typeBox.TabIndex = 10;
            // 
            // DepartLabel
            // 
            this.DepartLabel.AutoSize = true;
            this.DepartLabel.Location = new System.Drawing.Point(89, 480);
            this.DepartLabel.Name = "DepartLabel";
            this.DepartLabel.Size = new System.Drawing.Size(74, 13);
            this.DepartLabel.TabIndex = 11;
            this.DepartLabel.Text = "Отправление";
            // 
            // DestPlaceLabel
            // 
            this.DestPlaceLabel.AutoSize = true;
            this.DestPlaceLabel.Location = new System.Drawing.Point(92, 520);
            this.DestPlaceLabel.Name = "DestPlaceLabel";
            this.DestPlaceLabel.Size = new System.Drawing.Size(68, 13);
            this.DestPlaceLabel.TabIndex = 12;
            this.DestPlaceLabel.Text = "Назначение";
            // 
            // DepartTimeLabel
            // 
            this.DepartTimeLabel.AutoSize = true;
            this.DepartTimeLabel.Location = new System.Drawing.Point(354, 480);
            this.DepartTimeLabel.Name = "DepartTimeLabel";
            this.DepartTimeLabel.Size = new System.Drawing.Size(69, 13);
            this.DepartTimeLabel.TabIndex = 13;
            this.DepartTimeLabel.Text = "Время отпр.";
            // 
            // PathTimeLabel
            // 
            this.PathTimeLabel.AutoSize = true;
            this.PathTimeLabel.Location = new System.Drawing.Point(354, 521);
            this.PathTimeLabel.Name = "PathTimeLabel";
            this.PathTimeLabel.Size = new System.Drawing.Size(74, 13);
            this.PathTimeLabel.TabIndex = 14;
            this.PathTimeLabel.Text = "Время в пути";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 565);
            this.Controls.Add(this.PathTimeLabel);
            this.Controls.Add(this.DepartTimeLabel);
            this.Controls.Add(this.DestPlaceLabel);
            this.Controls.Add(this.DepartLabel);
            this.Controls.Add(this.typeBox);
            this.Controls.Add(this.typeLabel);
            this.Controls.Add(this.PathTimeBox);
            this.Controls.Add(this.DestPlaceBox);
            this.Controls.Add(this.DepartTimeBox);
            this.Controls.Add(this.DepartPlaceBox);
            this.Controls.Add(this.firstLabel);
            this.Controls.Add(this.firstBox);
            this.Controls.Add(this.AddButton);
            this.Controls.Add(this.textBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button AddButton;
        private System.Windows.Forms.TextBox firstBox;
        private System.Windows.Forms.Label firstLabel;
        private System.Windows.Forms.TextBox DepartPlaceBox;
        private System.Windows.Forms.TextBox DepartTimeBox;
        private System.Windows.Forms.TextBox DestPlaceBox;
        private System.Windows.Forms.TextBox PathTimeBox;
        private System.Windows.Forms.Label typeLabel;
        private System.Windows.Forms.ComboBox typeBox;
        private System.Windows.Forms.Label DepartLabel;
        private System.Windows.Forms.Label DestPlaceLabel;
        private System.Windows.Forms.Label DepartTimeLabel;
        private System.Windows.Forms.Label PathTimeLabel;
    }
}

