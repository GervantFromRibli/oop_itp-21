﻿using System;
using System.Windows.Forms;

namespace Lab4Form
{
    static class FormStart
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Paint());
        }
    }
}
