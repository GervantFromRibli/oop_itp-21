﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Collection
{
    public class CycleBuffer<T> : ICollection<T>
    {
        private T[] buffer;
        private int position;
        private bool check_full = false;
        public T this[int index]
        {
            get
            {
                while (index < 0)
                {
                    index += buffer.Length;
                }
                while (index >= buffer.Length)
                {
                    index -= buffer.Length;
                }
                return buffer[index];
            }
        }
        public CycleBuffer(T elem, int number)
        {
            buffer = new T[number];
            position = 0;
            Add(elem);
        }
        public CycleBuffer(int number)
        {
            buffer = new T[number];
            position = 0;
        }
        public CycleBuffer(T[] coll, int number)
        {
            buffer = new T[number];
            position = 0;
            Add(coll);
        }
        public int Count 
        {
            get
            {
                int count;
                if (check_full == true)
                {
                    count = buffer.Length;
                }
                else
                {
                    count = position;
                }
                return count;
            }
        }
        public bool IsReadOnly 
        {
            get
            {
                return false;
            }
        }

        public void Add(T item)
        {
            buffer[position] = item;
            position++;
            if (position == buffer.Length)
            {
                position = 0;
                check_full = true;
            }
        }
        public void Add(T[] coll)
        {
            foreach (var elem in coll)
            {
                Add(elem);
            }
        }
        public void Clear()
        {
            for (int i = 0; i < buffer.Length; i++)
            {
                buffer[i] = default;
                position = 0;
                check_full = false;
            }
        }

        public bool Contains(T item)
        {
            bool check = false;
            foreach (var elem in buffer)
            {
                if (elem.GetHashCode() == item.GetHashCode())
                {
                    check = true;
                }
            }
            return check;
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        public IEnumerator<T> GetEnumerator()
        {
            for (int i = 0; i < Count; i++)
            {
                yield return buffer[i];
            }
        }

        public bool Remove(T item)
        {
            throw new NotImplementedException();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
