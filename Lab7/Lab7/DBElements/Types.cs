﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBElements
{
    /// <summary>
    /// Class that represent "Type" type of data
    /// </summary>
    public class Types
    {
        private int id;
        /// <summary>
        /// Property to check the incoming id
        /// </summary>
        public int Id
        {
            get => id;
            set
            {
                if (value.ToString().Length != 1)
                {
                    throw new Exception("Wrong id");
                }
                else
                {
                    id = value;
                }
            }
        }
        /// <summary>
        /// Name of type
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Constructor to create "Types" object
        /// </summary>
        /// <param name="id"> Number of type </param>
        /// <param name="name"> Name of type </param>
        public Types(int id, string name)
        {
            Id = id;
            Name = name;
        }
    }
}
