﻿using System;
using System.Collections.Generic;
using System.Linq;
using DBElements;
using System.Data.Linq;

namespace DAL
{
    public class TownDAO : IDAO<Towns>
    {
        public string connectionString = @"Data Source=.\SQLEXPRESS;Initial Catalog=Trains; Integrated Security=true";

        public void Create(Towns obj)
        {
            DataContext db = new DataContext(connectionString);
            db.GetTable<Towns>().InsertOnSubmit(obj);
            db.SubmitChanges();
        }

        public void Delete(int id)
        {
            if (id.ToString().Length != 2)
            {
                throw new Exception("Wrong Id");
            }
            else
            {
                DataContext db = new DataContext(connectionString);
                var elem = db.GetTable<Towns>().Select(town => town).Where(town => town.Id == id).SingleOrDefault();
                db.GetTable<Towns>().DeleteOnSubmit(elem);
                db.SubmitChanges();
            }
        }

        public List<Towns> GetAll()
        {
            DataContext db = new DataContext(connectionString);
            Table<Towns> elems = db.GetTable<Towns>();
            List<Towns> objects_list = new List<Towns>();
            foreach (var elem in elems.ToList())
            {
                elem.Name = elem.Name.Trim();
                objects_list.Add(elem);
            }
            return objects_list;
        }

        public Towns Read(int id)
        {
            if (id.ToString().Length != 2)
            {
                throw new Exception("Wrong ID");
            }
            else
            {
                DataContext db = new DataContext(connectionString);
                var elem = db.GetTable<Towns>().Select(town => town).Where(town => town.Id == id).SingleOrDefault();
                elem.Name = elem.Name.Trim();
                return elem;
            }
        }
        public void Update(Towns obj)
        {
            DataContext db = new DataContext(connectionString);
            var elem = db.GetTable<Towns>().Select(town => town).Where(town => town.Id == obj.Id).SingleOrDefault();
            elem.Name = obj.Name;
            db.SubmitChanges();
        }
    }
}
