﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Lab4FileRead
{
    public static class TextClass
    {
        public static Dictionary<int, int> GetData(string path)
        {
            Dictionary<int, int> data = new Dictionary<int, int>();
            using (StreamReader reader = File.OpenText(path))
            {
                string s;
                while ((s = reader.ReadLine()) != null)
                {
                    string[] dataElem = s.Split(';');
                    if (dataElem.Length != 2)
                    {
                        throw new Exception();
                    }
                    else
                    {
                        data.Add(Convert.ToInt32(dataElem[0]), Convert.ToInt32(dataElem[1]));
                    }
                }
            }
            data.OrderBy(key => key.Key);
            return data;
        }
    }
}
