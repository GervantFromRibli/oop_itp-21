﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Lab1Integral;

namespace Lab1Tests
{
    [TestClass]
    public class IntegralTests
    {
        [TestMethod]
        public void CreateIntegral()
        {
            Integral integral = new Integral(3, 5, x => x + x*x);
            Assert.AreEqual(15, integral.A * integral.B);
        }
        [ExpectedException(typeof(Exception))]
        [TestMethod]
        public void CreateIntegral2()
        {
            Integral integral = new Integral(6, 6, x => x + x * x);
        }
        [ExpectedException(typeof(Exception))]
        [TestMethod]
        public void CreateIntegral3()
        {
            Integral integral = new Integral(7, 3, x => x + x * x);
        }
        [TestMethod]
        public void GetIntegral1()
        {
            Integral integral = new Integral(3, 7, x => x + x * x);
            float result = integral.GetIntegralByTrap();
            bool check = false;
            if (result > 125.33 && result < 125.34)
            {
                check = true;
            }
            Assert.AreEqual(true, check);
        }
        [ExpectedException(typeof(Exception))]
        [TestMethod]
        public void GetIntegral2()
        {
            Integral integral = new Integral(3, 7, null);
            float result = integral.GetIntegralByTrap();
        }
    }
}
