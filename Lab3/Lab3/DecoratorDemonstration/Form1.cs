﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using Lab3Stream;

namespace DecoratorDemonstration
{
    public partial class Form1 : Form
    {
        LabStream streamwriter = new LabStream("d:\\text.txt", Stream.Null);
        public Form1()
        {
            InitializeComponent();
            GetData();
        }
        public void GetData()
        {
            ShowBox.Clear();
            using (StreamReader reader = new StreamReader("d:\\text.txt"))
            {
                while (reader.Peek() >= 0)
                {
                    ShowBox.Text += reader.ReadLine() + Environment.NewLine;
                }
            }
        }

        private void StreamClick(object sender, EventArgs e)
        {
            List<byte> data = new List<byte>();
            foreach (var elem in text.Text)
            {
                data.Add(Convert.ToByte(elem));
            }
            FileStream file = new FileStream("d:\\text.txt", FileMode.Append);
            file.Write(data.ToArray(), 0, data.Count);
            file.Close();
            GetData();
        }

        private void LabStreamButton_Click(object sender, EventArgs e)
        {
            List<byte> data = new List<byte>();
            foreach (var elem in text.Text)
            {
                data.Add(Convert.ToByte(elem));
            }
            streamwriter.Write(data.ToArray(), 0, data.Count);
            GetData();
        }
    }
}
