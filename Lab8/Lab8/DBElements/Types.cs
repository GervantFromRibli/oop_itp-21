﻿using System;
using System.Data.Linq.Mapping;

namespace DBElements
{
    /// <summary>
    /// Class that represent "Type" type of data
    /// </summary>
    [Table(Name = "Types")]
    public class Types
    {
        private int id;
        /// <summary>
        /// Property to check the incoming id
        /// </summary>
        [Column(Name = "TypeId", CanBeNull = false, IsDbGenerated = false, Storage = "id", IsPrimaryKey = true)]
        public int Id
        {
            get => id;
            set
            {
                if (value.ToString().Length != 1)
                {
                    throw new Exception("Wrong id");
                }
                else
                {
                    id = value;
                }
            }
        }
        /// <summary>
        /// Name of type
        /// </summary>
        [Column(Name = "Name", CanBeNull = false, IsDbGenerated = false)]
        public string Name { get; set; }
        /// <summary>
        /// Constructor to create "Types" object
        /// </summary>
        /// <param name="id"> Number of type </param>
        /// <param name="name"> Name of type </param>
        public Types(int id, string name)
        {
            Id = id;
            Name = name;
        }
        /// <summary>
        /// Constructor to create "Types" object
        /// </summary>
        public Types() { }
    }
}
