﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBElements
{
    /// <summary>
    /// Class that represent the time of train
    /// </summary>
    public class TimeClass
    {
        /// <summary>
        /// Hour of time
        /// </summary>
        public int hour;
        /// <summary>
        /// Minute of time
        /// </summary>
        public int minute;
        /// <summary>
        /// Constructor for creating a time
        /// </summary>
        /// <param name="hour"></param>
        /// <param name="minute"></param>
        public TimeClass(int hour, int minute)
        {
            this.hour = hour;
            this.minute = minute;
        }
        /// <summary>
        /// Method to return the time
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return hour.ToString() + ":" + minute.ToString();
        }
    }
}
