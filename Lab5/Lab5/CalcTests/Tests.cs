﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Lab5Calc;

namespace CalcTests
{
    [TestClass]
    public class Tests
    {
        Calculator calculator = new Calculator();
        [TestMethod]
        public void PlusMethod()
        {
            List<string> plusNumb = new List<string>();
            float[] check_res = new float[] { 160, 12, 20, 23 };
            plusNumb.Add("135 + 25");
            plusNumb.Add("3+ 9");
            plusNumb.Add("13 +7");
            plusNumb.Add(" 19+4 ");
            float[] result = calculator.GetResult(plusNumb);
            int check = 0;
            for (int i = 0; i < result.Length; i++)
            {
                if (check_res[i] == result[i])
                {
                    check++;
                }
            }
            Assert.AreEqual(4, check);
        }
        [TestMethod]
        public void MinusMethod()
        {
            List<string> plusNumb = new List<string>();
            float[] check_res = new float[] { 110, -6, 6, 15 };
            plusNumb.Add("135 - 25");
            plusNumb.Add("3- 9");
            plusNumb.Add("13 -7");
            plusNumb.Add(" 19-4 ");
            float[] result = calculator.GetResult(plusNumb);
            int check = 0;
            for (int i = 0; i < result.Length; i++)
            {
                if (check_res[i] == result[i])
                {
                    check++;
                }
            }
            Assert.AreEqual(4, check);
        }
        [TestMethod]
        public void MultiMethod()
        {
            List<string> plusNumb = new List<string>();
            float[] check_res = new float[] { 3375, 27, 91, 76 };
            plusNumb.Add("135 * 25");
            plusNumb.Add("3* 9");
            plusNumb.Add("13 *7");
            plusNumb.Add(" 19*4 ");
            float[] result = calculator.GetResult(plusNumb);
            int check = 0;
            for (int i = 0; i < result.Length; i++)
            {
                if (check_res[i] == result[i])
                {
                    check++;
                }
            }
            Assert.AreEqual(4, check);
        }
        [TestMethod]
        public void DivideMethod()
        {
            List<string> plusNumb = new List<string>();
            float[] check_res = new float[] { 5.4F, 3, 2, 4.75F };
            plusNumb.Add("135 / 25");
            plusNumb.Add("9/ 3");
            plusNumb.Add("14 /7");
            plusNumb.Add(" 19/4 ");
            float[] result = calculator.GetResult(plusNumb);
            int check = 0;
            for (int i = 0; i < result.Length; i++)
            {
                if (check_res[i] == result[i])
                {
                    check++;
                }
            }
            Assert.AreEqual(4, check);
        }
        [ExpectedException(typeof(FormatException))]
        [TestMethod]
        public void CheckErr()
        {
            List<string> plusNumb = new List<string>();
            plusNumb.Add("13525");
            plusNumb.Add("9/ 3");
            plusNumb.Add("14 /7");
            plusNumb.Add(" 194 ");
            float[] result = calculator.GetResult(plusNumb);
        }
    }
}
