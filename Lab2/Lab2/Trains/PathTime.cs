﻿using System;

namespace Trains
{
    public class PathTime
    {
        private int hour = 0;
        public int Hour
        {
            get => hour;
            set
            {
                if (value < 0)
                {
                    throw new Exception("Неправильный час");
                }
                else
                {
                    hour = value;
                }
            }
        }
        public int minutes = 0;
        public int Minutes
        {
            get => minutes;
            set
            {
                if (value < 0 || value > 59)
                {
                    throw new Exception("Неправильные минуты");
                }
                else
                {
                    minutes = value;
                }
            }
        }
        public PathTime() { }
        public PathTime(int Hour, int Minutes)
        {
            this.Hour = Hour;
            this.Minutes = Minutes;
        }
        public override string ToString()
        {
            return Hour.ToString() + ":" + Minutes.ToString();
        }
    }
}
