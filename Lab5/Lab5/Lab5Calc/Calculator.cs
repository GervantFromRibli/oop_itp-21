﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Lab5Calc
{
    public class Calculator
    {
        Dictionary<char, Func<string, float>> functions;
        public Calculator()
        {
            functions = new Dictionary<char, Func<string, float>>();
            functions.Add('+', (x) => Convert.ToSingle(x.Trim().Split('+')[0].Trim()) + Convert.ToSingle(x.Trim().Split('+')[1].Trim()));
            functions.Add('-', (x) => Convert.ToSingle(x.Trim().Split('-')[0].Trim()) - Convert.ToSingle(x.Trim().Split('-')[1].Trim()));
            functions.Add('*', (x) => Convert.ToSingle(x.Trim().Split('*')[0].Trim()) * Convert.ToSingle(x.Trim().Split('*')[1].Trim()));
            functions.Add('/', (x) => Convert.ToSingle(x.Trim().Split('/')[0].Trim()) / Convert.ToSingle(x.Trim().Split('/')[1].Trim()));
        }
        public float[] GetResult(List<string> data)
        {
            List<float> result = new List<float>();
            Regex regex = new Regex(@"[^\s\d]");
            foreach (var elem in data)
            {
                Match match = regex.Match(elem);
                result.Add(functions[Convert.ToChar(match.Value)](elem));
            }
            return result.ToArray();
        }
    }
}
