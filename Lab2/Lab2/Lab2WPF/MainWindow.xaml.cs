﻿using System;
using System.Collections.Generic;
using System.Windows;
using Lab2DOM;
using Trains;

namespace Lab2WPF
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            GetTrains();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            int number = Convert.ToInt32(Number.Text);
            string departPlace = DepartPlace.Text;
            string destPlace = DestPlace.Text;
            string[] depart = DepartTime.Text.Split(':');
            PathTime departTime = new PathTime();
            PathTime pathTime = new PathTime();
            string list = "";
            if (depart.Length != 2)
            {
                throw new Exception("Неправильное время");
            }
            else
            {
                departTime.Minutes = Convert.ToInt32(depart[1]);
                departTime.Hour = Convert.ToInt32(depart[0]);
            }
            string[] timePath = TimePath.Text.Split(':');
            if (timePath.Length != 2)
            {
                throw new Exception("Неправильное время");
            }
            else
            {
                pathTime.Minutes = Convert.ToInt32(timePath[1]);
                pathTime.Hour = Convert.ToInt32(timePath[0]);
            }
            Types type = (Types)Enum.Parse(typeof(Types), Type.Text);
            list = ListStop.Text;
            List<Train> trains = DOMTree.ReadDoc();
            bool check = true;
            for (int i = 0; i < trains.Count; i++)
            {
                if (trains[i].Number == number)
                {
                    trains[i].DepartPlace = departPlace;
                    trains[i].Destination = destPlace;
                    trains[i].departTime = departTime;
                    trains[i].pathTime = pathTime;
                    trains[i].type = type;
                    trains[i].ListOfStop = list;
                    check = false;
                }
            }
            if (check)
            {
                trains.Add(new Train(number, destPlace, departPlace, departTime.Hour, departTime.Minutes, pathTime.Hour, pathTime.Minutes, type, list)); ;
            }
            DOMTree.CreateDoc(trains);
            GetTrains();
        }
        private void GetTrains()
        {
            Trains.Items.Clear();
            foreach (var elem in DOMTree.ReadDoc())
            {
                Trains.Items.Add(elem.ToString());
            }
        }
    }
}
