﻿using System;
using System.IO;
using System.Windows.Forms;

namespace Lab3Stream
{
    public class LabStream : Stream
    {
        protected readonly Stream stream;
        private string path;
        public override bool CanRead => throw new NotImplementedException();

        public override bool CanSeek => throw new NotImplementedException();

        public override bool CanWrite => throw new NotImplementedException();

        public override long Length => throw new NotImplementedException();

        public override long Position { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public LabStream(string path, Stream stream)
        {
            this.stream = stream;
            this.path = path;
        }
        public override void Flush()
        {
            throw new NotImplementedException();
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            throw new NotImplementedException();
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            throw new NotImplementedException();
        }

        public override void SetLength(long value)
        {
            throw new NotImplementedException();
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            string new_string = Clipboard.GetText();
            for (int i = 0; i < count; i++)
            {
                new_string += Convert.ToChar(buffer[i + offset]).ToString();
            }
            Clipboard.SetText(new_string);
            if (Clipboard.GetText().Length >= 100)
            {
                File.AppendAllText(path, Clipboard.GetText() + "\n");
                Clipboard.Clear();
            }
        }
    }
}
