﻿using DBElements;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Commands;

namespace DAL
{
    public class TrainDAO : IDAO<Train>
    {
        public string connectionString = @"Data Source=.\SQLEXPRESS;Initial Catalog=Trains; Integrated Security=true";
        public void Create(Train obj)
        {
            string command = $"INSERT INTO [Train] (TrainNumber, DepartPlace, ArrivePlace, DepartTime, PathTime, Type) VALUES (@Id, @depart, @arrive, @depTime, @pathTime, @Type)";
            SqlCommand cmd = new SqlCommand(command);
            cmd.Parameters.AddWithValue("@Id", obj.Id);
            cmd.Parameters.AddWithValue("@depart", Comm.GetCertainId(new TownDAO().GetAll(), obj.DepartPlace));
            cmd.Parameters.AddWithValue("@arrive", Comm.GetCertainId(new TownDAO().GetAll(), obj.ArrivePlace));
            cmd.Parameters.AddWithValue("@depTime", new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, obj.DepartTime.hour, obj.DepartTime.minute, 0));
            cmd.Parameters.AddWithValue("@pathTime", new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, obj.PathTime.hour, obj.PathTime.minute, 0));
            cmd.Parameters.AddWithValue("@Type", Comm.GetCertainId(new TypesDAO().GetAll(), obj.type));
            SqlConnection connection = new SqlConnection(connectionString);
            connection.Open();
            cmd.Connection = connection;
            cmd.ExecuteNonQuery();
            connection.Close();
        }

        public void Delete(int id)
        {
            if (id.ToString().Length != 3)
            {
                throw new Exception("Wrong Id");
            }
            else
            {
                string command = $"Delete from [Train] where TrainNumber={id}";
                SqlCommand cmd = new SqlCommand(command);
                SqlConnection connection = new SqlConnection(connectionString);
                connection.Open();
                cmd.Connection = connection;
                cmd.ExecuteNonQuery();
                connection.Close();
            }
        }

        public List<Train> GetAll()
        {
            string command = $"SELECT * FROM [Train]";
            SqlCommand cmd = new SqlCommand(command);
            SqlConnection connection = new SqlConnection(connectionString);
            connection.Open();
            cmd.Connection = connection;
            SqlDataReader dbreader = cmd.ExecuteReader();
            List<Train> objects_list = new List<Train>();
            while (dbreader.Read())
            {
                Train elem = new Train(dbreader.GetInt32(0), Comm.GetCertainName(new TownDAO().GetAll(), dbreader.GetInt32(1)), Comm.GetCertainName(new TownDAO().GetAll(), dbreader.GetInt32(2)), new TimeClass(dbreader.GetDateTime(3).Hour, dbreader.GetDateTime(3).Minute), new TimeClass(dbreader.GetDateTime(4).Hour, dbreader.GetDateTime(4).Minute), Comm.GetCertainName(new TypesDAO().GetAll(), dbreader.GetInt32(5)));
                objects_list.Add(elem);
            }
            connection.Close();
            return objects_list;
        }

        public Train Read(int id)
        {
            if (id.ToString().Length != 3)
            {
                throw new Exception("Wrong ID");
            }
            else
            {
                string command = $"SELECT * FROM [Train] WHERE [TrainNumber]={id}";
                SqlCommand cmd = new SqlCommand(command);
                SqlConnection connection = new SqlConnection(connectionString);
                connection.Open();
                Train elem = null;
                cmd.Connection = connection;
                SqlDataReader dbreader = cmd.ExecuteReader();
                if (dbreader.Read())
                {
                    elem = new Train(dbreader.GetInt32(0), Comm.GetCertainName(new TownDAO().GetAll(), dbreader.GetInt32(1)), Comm.GetCertainName(new TownDAO().GetAll(), dbreader.GetInt32(2)), new TimeClass(dbreader.GetDateTime(3).Hour, dbreader.GetDateTime(3).Minute), new TimeClass(dbreader.GetDateTime(4).Hour, dbreader.GetDateTime(4).Minute), Comm.GetCertainName(new TypesDAO().GetAll(), dbreader.GetInt32(5)));
                }
                connection.Close();
                return elem;
            }
        }

        public void Update(Train obj)
        {
            string command = $"UPDATE [Train] SET DepartPlace=@depart, ArrivePlace=@arrive, DepartTime=@depTime, PathTime=@path, Type=@type WHERE TrainNumber=@Id";
            SqlCommand cmd = new SqlCommand(command);
            cmd.Parameters.AddWithValue("@Id", obj.Id);
            cmd.Parameters.AddWithValue("@depart", Comm.GetCertainId(new TownDAO().GetAll(), obj.DepartPlace));
            cmd.Parameters.AddWithValue("@arrive", Comm.GetCertainId(new TownDAO().GetAll(), obj.ArrivePlace));
            cmd.Parameters.AddWithValue("@depTime", new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, obj.DepartTime.hour, obj.DepartTime.minute, 0));
            cmd.Parameters.AddWithValue("@path", new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, obj.PathTime.hour, obj.PathTime.minute, 0));
            cmd.Parameters.AddWithValue("@type", Comm.GetCertainId(new TypesDAO().GetAll(), obj.type));
            SqlConnection connection = new SqlConnection(connectionString);
            connection.Open();
            cmd.Connection = connection;
            cmd.ExecuteNonQuery();
            connection.Close();
        }
    }
}
