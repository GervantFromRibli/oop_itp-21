﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBElements
{
    /// <summary>
    /// Class for "Towns" type of data
    /// </summary>
    public class Towns
    {
        private int id;
        /// <summary>
        /// Property to check incoming id
        /// </summary>
        public int Id
        {
            get => id;
            set
            {
                if (value.ToString().Length != 2)
                {
                    throw new Exception("Wrong Id");
                }
                else
                {
                    id = value;
                }
            }
        }
        /// <summary>
        /// Name of town
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Constructor for creating "Towns" object
        /// </summary>
        /// <param name="id"> Id of town </param>
        /// <param name="name"> Name of town </param>
        public Towns(int id, string name)
        {
            Id = id;
            Name = name;
        }
    }
}
