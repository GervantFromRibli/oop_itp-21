USE [master]
GO
/****** Object:  Database [Trains]    Script Date: 13.06.2020 20:05:40 ******/
CREATE DATABASE [Trains]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Trains', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\Trains.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'Trains_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\Trains_log.ldf' , SIZE = 2048KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [Trains] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Trains].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Trains] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Trains] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Trains] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Trains] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Trains] SET ARITHABORT OFF 
GO
ALTER DATABASE [Trains] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Trains] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Trains] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Trains] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Trains] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Trains] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Trains] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Trains] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Trains] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Trains] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Trains] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Trains] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Trains] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Trains] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Trains] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Trains] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Trains] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Trains] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Trains] SET  MULTI_USER 
GO
ALTER DATABASE [Trains] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Trains] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Trains] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Trains] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [Trains] SET DELAYED_DURABILITY = DISABLED 
GO
USE [Trains]
GO
/****** Object:  Table [dbo].[Towns]    Script Date: 13.06.2020 20:05:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Towns](
	[TownId] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Towns] PRIMARY KEY CLUSTERED 
(
	[TownId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Train]    Script Date: 13.06.2020 20:05:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Train](
	[TrainNumber] [int] NOT NULL,
	[DepartPlace] [int] NOT NULL,
	[ArrivePlace] [int] NOT NULL,
	[DepartTime] [datetime] NOT NULL,
	[PathTime] [datetime] NOT NULL,
	[Type] [int] NOT NULL,
 CONSTRAINT [PK_Train] PRIMARY KEY CLUSTERED 
(
	[TrainNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Types]    Script Date: 13.06.2020 20:05:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Types](
	[TypeId] [int] NOT NULL,
	[Name] [nvarchar](20) NOT NULL,
 CONSTRAINT [PK_Types] PRIMARY KEY CLUSTERED 
(
	[TypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[Train]  WITH CHECK ADD  CONSTRAINT [FK_Train_Towns] FOREIGN KEY([DepartPlace])
REFERENCES [dbo].[Towns] ([TownId])
GO
ALTER TABLE [dbo].[Train] CHECK CONSTRAINT [FK_Train_Towns]
GO
ALTER TABLE [dbo].[Train]  WITH CHECK ADD  CONSTRAINT [FK_Train_Towns1] FOREIGN KEY([ArrivePlace])
REFERENCES [dbo].[Towns] ([TownId])
GO
ALTER TABLE [dbo].[Train] CHECK CONSTRAINT [FK_Train_Towns1]
GO
ALTER TABLE [dbo].[Train]  WITH CHECK ADD  CONSTRAINT [FK_Train_Types] FOREIGN KEY([Type])
REFERENCES [dbo].[Types] ([TypeId])
GO
ALTER TABLE [dbo].[Train] CHECK CONSTRAINT [FK_Train_Types]
GO
USE [master]
GO
ALTER DATABASE [Trains] SET  READ_WRITE 
GO
