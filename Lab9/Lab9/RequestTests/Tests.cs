﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Lab9Linq;
using System.Collections.Generic;
using System.Linq;

namespace RequestTests
{
    [TestClass]
    public class Tests
    {
        [TestMethod]
        public void Request1()
        {
            File.WriteAllText("d:\\lab9.txt", "dawawda; dawawda Купил Скайрим?dawawda.wdawdwadia]wdadw/dawawda купил скайрим");
            var Words = Request.TaskRequest();
            Dictionary<string, int> keyValues = new Dictionary<string, int>();
            foreach (var group in Words)
            {
                if (group.Key != null)
                {
                    keyValues.Add(group.Key, group.Count());
                }
            }
            Assert.AreEqual(keyValues["купил"], 2);
        }
        [TestMethod]
        public void Request2()
        {
            File.WriteAllText("d:\\lab9.txt", "dawawda; dawawda Купил Скайрим?dawawda.wdawdwadia]wdadw/dawawda купил скайрим aaaaaa bbbbbb ssssss dddddddd kkk");
            var Words = Request.TaskRequest();
            Dictionary<string, int> keyValues = new Dictionary<string, int>();
            foreach (var group in Words)
            {
                if (group.Key != null)
                {
                    keyValues.Add(group.Key, group.Count());
                }
            }
            Assert.AreEqual(keyValues.ContainsKey("kkk"), false);
        }
    }
}
