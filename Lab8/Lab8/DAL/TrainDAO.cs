﻿using DBElements;
using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;

namespace DAL
{
    public class TrainDAO : IDAO<Train>
    {
        public string connectionString = @"Data Source=.\SQLEXPRESS;Initial Catalog=Trains; Integrated Security=true";
        public void Create(Train obj)
        {
            DataContext db = new DataContext(connectionString);
            db.GetTable<Train>().InsertOnSubmit(obj);
            db.SubmitChanges();
        }

        public void Delete(int id)
        {
            if (id.ToString().Length != 3)
            {
                throw new Exception("Wrong Id");
            }
            else
            {
                DataContext db = new DataContext(connectionString);
                var elem = db.GetTable<Train>().Select(train => train).Where(train => train.Id == id).SingleOrDefault();
                db.GetTable<Train>().DeleteOnSubmit(elem);
                db.SubmitChanges();
            }
        }

        public List<Train> GetAll()
        {
            DataContext db = new DataContext(connectionString);
            Table<Train> elems = db.GetTable<Train>();
            List<Train> objects_list = new List<Train>();
            return objects_list;
        }

        public Train Read(int id)
        {
            if (id.ToString().Length != 3)
            {
                throw new Exception("Wrong ID");
            }
            else
            {
                DataContext db = new DataContext(connectionString);
                var elem = db.GetTable<Train>().Select(train => train).Where(train => train.Id == id).SingleOrDefault();
                return elem;
            }
        }

        public void Update(Train obj)
        {
            DataContext db = new DataContext(connectionString);
            var elem = db.GetTable<Train>().Select(train => train).Where(train => train.Id == obj.Id).SingleOrDefault();
            elem.DepartPlace = obj.DepartPlace;
            elem.ArrivePlace = obj.ArrivePlace;
            elem.DepartTime = obj.DepartTime;
            elem.PathTime = obj.PathTime;
            elem.type = obj.type;
            db.SubmitChanges();
        }
    }
}
