﻿using System.Collections.Generic;
using System.IO;

namespace Lab5File
{
    public static class FileReader
    {
        public static List<string> Read(string path)
        {
            List<string> list = new List<string>();
            using (StreamReader reader = new StreamReader(path))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    list.Add(line);
                }
            }
            return list;
        }
    }
}
