﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Lab4Form
{
    public partial class Paint : Form
    {
        Point startPoint = new Point();
        Point endPoint = new Point();
        Bitmap bitmap = new Bitmap(900, 450);
        Bitmap rectBorders = new Bitmap(900, 450);
        PictureBox paintBox = new PictureBox()
        {
            Image = null,
            Size = new Size(900, 450),
            Location = new Point(50, 20),
            BackColor = Color.AntiqueWhite
        };
        Button changeVersion = new Button()
        {
            Location = new Point(450, 500),
            Text = "Ластик"
        };
        public Paint()
        {
            InitializeComponent();
            Width = 1000;
            Height = 600;
            Controls.Add(paintBox);
            Controls.Add(changeVersion);
            paintBox.MouseDown += StartDraw;
            paintBox.MouseUp += DrawRect;
            changeVersion.Click += EraserOrBrush;
        }
        private void StartDraw(object sender, MouseEventArgs e)
        {
            startPoint = new Point(e.X, e.Y);
            paintBox.MouseMove += SizeRect;
        }
        private void SizeRect(object sender, MouseEventArgs e)
        {
            endPoint = new Point(e.X, e.Y);
            rectBorders = new Bitmap(DrawClass.Draw(new Bitmap(bitmap), startPoint, endPoint));
            paintBox.Image = rectBorders;
        }
        private void DrawRect(object sender, MouseEventArgs e)
        {
            bitmap = new Bitmap(DrawClass.Draw(bitmap, startPoint, endPoint));
            paintBox.Image = bitmap;
            paintBox.MouseMove -= SizeRect;
        }
        private void EraserOrBrush(object sender, EventArgs e)
        {
            DrawClass.ChangeBrush();
            changeVersion.Text = changeVersion.Text == "Ластик" ? changeVersion.Text = "Заливка" : changeVersion.Text = "Ластик";
        }
    }
}
