﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DAL;
using DBElements;
using Commands;
using System.Linq;

namespace DataAccessTests
{
    [TestClass]
    public class Trains
    {
        /// <summary>
        /// Необходимо убрать все записи из таблицы "Поезда"
        /// </summary>
        [TestMethod]
        public void GetAllTrains()
        {
            var trains = new TrainDAO().GetAll();
            Assert.AreEqual(0, trains.Count);
        }
        /// <summary>
        /// Необходимо иметь хотя бы одну запись в таблице "Города"
        /// </summary>
        [TestMethod]
        public void CreateTrain()
        {
            var town = Comm.GetNames(new TownDAO().GetAll()).First();
            new TrainDAO().Create(new Train(100, town, town, new TimeClass(1, 1), new TimeClass(1, 1), "Пассажирский"));
            var trains = new TrainDAO().GetAll();
            Assert.AreEqual(1, trains.Count);
        }
        [TestMethod]
        public void ReadTrain()
        {
            var train = new TrainDAO().Read(100);
            var town = Comm.GetNames(new TownDAO().GetAll()).First();
            Assert.AreEqual(town, train.ArrivePlace);
        }
        [TestMethod]
        public void UpdateTrain()
        {
            var town = Comm.GetNames(new TownDAO().GetAll()).First();
            new TrainDAO().Update(new Train(100, town, town, new TimeClass(1, 1), new TimeClass(1, 1), "Скорый"));
            var train = new TrainDAO().Read(100);
            Assert.AreEqual("Скорый", train.type);
        }
        [TestMethod]
        public void DeleteTrain()
        {
            new TrainDAO().Delete(100);
            var train = new TrainDAO().GetAll();
            Assert.AreEqual(0, train.Count);
        }
    }
    [TestClass]
    public class TownsTests
    {
        /// <summary>
        /// Необходимо убрать все записи из таблицы "Города"
        /// </summary>
        [TestMethod]
        public void GetAllTowns()
        {
            var towns = new TownDAO().GetAll();
            Assert.AreEqual(0, towns.Count);
        }
        [TestMethod]
        public void CreateTown()
        {
            new TownDAO().Create(new Towns(24, "dawdwdawd"));
            var towns = new TownDAO().GetAll();
            Assert.AreEqual(1, towns.Count);
        }
        [TestMethod]
        public void ReadTown()
        {
            var town = new TownDAO().Read(24);
            Assert.AreEqual("dawdwdawd", town.Name);
        }
        [TestMethod]
        public void UpdateTrain()
        {
            new TownDAO().Update(new Towns(24, "aawawaaw"));
            var towns = new TownDAO().Read(24);
            Assert.AreEqual("aawawaaw", towns.Name);
        }
        [TestMethod]
        public void DeleteTrain()
        {
            new TownDAO().Delete(24);
            var towns = new TownDAO().GetAll();
            Assert.AreEqual(0, towns.Count);
        }
    }
    /// <summary>
    /// Так как по заданию типы являются справочной таблицей,
    /// то будут проверяться только методы чтения
    /// </summary>
    [TestClass]
    public class TypesTests
    {
        [TestMethod]
        public void GetAllTypes()
        {
            var types = new TypesDAO().GetAll();
            Assert.AreEqual(3, types.Count);
        }
        [TestMethod]
        public void ReadType()
        {
            var type = new TypesDAO().Read(2);
            Assert.AreEqual("Пассажирский", type.Name);
        }
    }
    /// <summary>
    /// Так как по заданию типы являются справочной таблицей,
    /// то будут проверяться только методы чтения
    /// </summary>
    [TestClass]
    public class CommTests
    {
        [TestMethod]
        public void GetTypeNames()
        {
            var types = Comm.GetNames(new TypesDAO().GetAll());
            Assert.AreEqual(3, types.Count());
        }
        [TestMethod]
        public void GetTownsNames()
        {
            var types = Comm.GetNames(new TownDAO().GetAll());
            Assert.AreEqual(1, types.Count());
        }
        [TestMethod]
        public void GetCertainTownName()
        {
            var types = Comm.GetCertainName(new TownDAO().GetAll(), 23);
            Assert.AreEqual("awadwdadwdaw", types);
        }
        [TestMethod]
        public void GetCertainTypeName()
        {
            var types = Comm.GetCertainName(new TypesDAO().GetAll(), 2);
            Assert.AreEqual("Пассажирский", types);
        }
        [TestMethod]
        public void GetCertainTypeId()
        {
            var types = Comm.GetCertainId(new TypesDAO().GetAll(), "Скорый");
            Assert.AreEqual(1, types);
        }
        [TestMethod]
        public void GetCertainTownId()
        {
            var types = Comm.GetCertainId(new TownDAO().GetAll(), "awadwdadwdaw");
            Assert.AreEqual(23, types);
        }
    }
}
