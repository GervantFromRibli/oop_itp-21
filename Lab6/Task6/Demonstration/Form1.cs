﻿using System;
using System.Drawing;
using System.Windows.Forms;
using TrainClass;
using Collection;

namespace Demonstration
{
    public partial class Form1 : Form
    {
        CycleBuffer<int> int_coll = new CycleBuffer<int>(20);
        CycleBuffer<string> string_coll = new CycleBuffer<string>(20);
        CycleBuffer<Train> trains = new CycleBuffer<Train>(20);
        Button numbButton = new Button()
        {
            Location = new Point(250, 300),
            Text = "Число",
            Size = new Size(100, 40)
        };
        Button stringButton = new Button()
        {
            Location = new Point(450, 300),
            Text = "Строка",
            Size = new Size(100, 40)
        };
        Button trainButton = new Button()
        {
            Location = new Point(650, 300),
            Text = "Поезд",
            Size = new Size(100, 40)
        };
        public Form1()
        {
            InitializeComponent();
            Controls.Clear();
            Controls.Add(numbButton);
            Controls.Add(stringButton);
            Controls.Add(trainButton);
            trainButton.Click += new EventHandler(CreateTrain);
            numbButton.Click += new EventHandler(CreateNumb);
            stringButton.Click += new EventHandler(CreateString);
        }
        public void CreateTrain(object sender, EventArgs e)
        {
            Controls.Clear();
            Controls.Add(textBox1);
            Controls.Add(firstBox);
            Controls.Add(firstLabel);
            Controls.Add(AddButton);
            AddButton.Click += new EventHandler(AddTrain);
            Controls.Add(firstBox);
            Controls.Add(firstLabel);
            Controls.Add(DepartLabel);
            Controls.Add(DepartPlaceBox);
            Controls.Add(DestPlaceBox);
            Controls.Add(DestPlaceLabel);
            Controls.Add(DepartTimeBox);
            Controls.Add(DepartTimeLabel);
            Controls.Add(typeBox);
            Controls.Add(typeLabel);
            Controls.Add(PathTimeLabel);
            Controls.Add(PathTimeBox);
            firstLabel.Text = "Номер поезда";
            typeBox.Items.AddRange(new string[] { "Пассажирский", "Скорый", "Экспресс" });
            ShowData();
        }
        public void CreateString(object sender, EventArgs e)
        {
            Controls.Clear();
            string_coll = new CycleBuffer<string>(20);
            Controls.Add(firstBox);
            Controls.Add(textBox1);
            Controls.Add(firstLabel);
            Controls.Add(AddButton);
            firstLabel.Text = "Строка";
            AddButton.Click += new EventHandler(AddString);
            ShowData();
        }
        public void CreateNumb(object sender, EventArgs e)
        {
            Controls.Clear();
            int_coll = new CycleBuffer<int>(20);
            Controls.Add(textBox1);
            Controls.Add(firstBox);
            Controls.Add(firstLabel);
            Controls.Add(AddButton);
            firstLabel.Text = "Число";
            AddButton.Click += new EventHandler(AddNumb);
            ShowData();
        }
        public void AddString(object sender, EventArgs e)
        {
            textBox1.Clear();
            string_coll.Add(firstBox.Text);
            ShowData();
        }
        public void AddNumb(object sender, EventArgs e)
        {
            textBox1.Clear();
            int_coll.Add(Convert.ToInt32(firstBox.Text));
            ShowData();
        }
        public void AddTrain(object sender, EventArgs e)
        {
            textBox1.Clear();
            string[] hour_depart = DepartTimeBox.Text.Split(':');
            string[] path_time = PathTimeBox.Text.Split(':');
            trains.Add(new Train(Convert.ToInt32(firstBox.Text), DestPlaceBox.Text, DepartPlaceBox.Text, Convert.ToInt32(hour_depart[0]), Convert.ToInt32(hour_depart[1]), Convert.ToInt32(path_time[0]), Convert.ToInt32(path_time[1]), (Types)Enum.Parse(typeof(Types), typeBox.SelectedItem.ToString())));
            ShowData();
        }
        public void ShowData()
        {
            if (int_coll.Count != 0)
            {
                foreach (var elem in int_coll)
                {
                    textBox1.Text += elem.ToString() + Environment.NewLine;
                }
            }
            else if (string_coll.Count != 0)
            {
                foreach (var elem in string_coll)
                {
                    textBox1.Text += elem.ToString() + Environment.NewLine;
                }
            }
            else if (trains.Count != 0)
            {
                foreach (var elem in trains)
                {
                    textBox1.Text += elem.ToString() + Environment.NewLine;
                }
            }
        }
    }
}
