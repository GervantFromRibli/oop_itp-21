﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBElements
{
    /// <summary>
    /// Class that represent the "Train" type of data
    /// </summary>
    public class Train
    {
        private int id;
        /// <summary>
        /// Property to check the incoming id
        /// </summary>
        public int Id
        {
            get => id;
            set
            {
                if (value.ToString().Length != 3)
                {
                    throw new Exception("Wrong Id");
                }
                else
                {
                    id = value;
                }
            }
        }
        /// <summary>
        /// Place of departure
        /// </summary>
        public string DepartPlace;
        /// <summary>
        /// Place of arriving
        /// </summary>
        public string ArrivePlace;
        private TimeClass departTime;
        /// <summary>
        /// Property to check the time of departure
        /// </summary>
        public TimeClass DepartTime
        {
            get => departTime;
            set
            {
                if (value.hour > 0 && value.hour < 24 && value.minute > 0 && value.minute < 60)
                {
                    departTime = value;
                }
                else
                {
                    throw new Exception("Wrong time");
                }
            }
        }
        private TimeClass pathTime;
        /// <summary>
        /// Property to check the path time
        /// </summary>
        public TimeClass PathTime
        {
            get => pathTime;
            set
            {
                if (value.hour > 0 && value.minute > 0 && value.minute < 60)
                {
                    pathTime = value;
                }
                else
                {
                    throw new Exception("Wrong time");
                }
            }
        }
        /// <summary>
        /// Type of train
        /// </summary>
        public string type;
        /// <summary>
        /// Constructor to create the "train" object
        /// </summary>
        /// <param name="id"> Number of train </param>
        /// <param name="departPlace"> Place of departure </param>
        /// <param name="arrivePlace"> Place of arriving </param>
        /// <param name="departTime"> Time of departure </param>
        /// <param name="pathTime"> Path time </param>
        /// <param name="type"> Type of train </param>
        public Train(int id, string departPlace, string arrivePlace, TimeClass departTime, TimeClass pathTime, string type)
        {
            Id = id;
            DepartPlace = departPlace;
            ArrivePlace = arrivePlace;
            DepartTime = departTime;
            PathTime = pathTime;
            this.type = type;
        }
    }
}
