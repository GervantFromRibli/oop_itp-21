﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DBElements;

namespace DAL
{
    public class TownDAO : IDAO<Towns>
    {
        public string connectionString = @"Data Source=.\SQLEXPRESS;Initial Catalog=Trains; Integrated Security=true";

        public void Create(Towns obj)
        {
            string command = $"INSERT INTO [Towns] (TownId, Name) VALUES (@Id, @Name)";
            SqlCommand cmd = new SqlCommand(command);
            cmd.Parameters.AddWithValue("@Id", obj.Id);
            cmd.Parameters.AddWithValue("@Name", obj.Name);
            SqlConnection connection = new SqlConnection(connectionString);
            connection.Open();
            cmd.Connection = connection;
            cmd.ExecuteNonQuery();
            connection.Close();
        }

        public void Delete(int id)
        {
            if (id.ToString().Length != 2)
            {
                throw new Exception("Wrong Id");
            }
            else
            {
                string command = $"Delete from [Towns] where TownId={id}";
                SqlCommand cmd = new SqlCommand(command);
                SqlConnection connection = new SqlConnection(connectionString);
                connection.Open();
                cmd.Connection = connection;
                cmd.ExecuteNonQuery();
                connection.Close();
            }
        }

        public List<Towns> GetAll()
        {
            string command = $"SELECT * FROM [Towns]";
            SqlCommand cmd = new SqlCommand(command);
            SqlConnection connection = new SqlConnection(connectionString);
            connection.Open();
            cmd.Connection = connection;
            SqlDataReader dbreader = cmd.ExecuteReader();
            List<Towns> objects_list = new List<Towns>();
            while (dbreader.Read())
            {
                Towns elem = new Towns(dbreader.GetInt32(0), dbreader.GetString(1));
                objects_list.Add(elem);
            }
            connection.Close();
            return objects_list;
        }

        public Towns Read(int id)
        {
            if (id.ToString().Length != 2)
            {
                throw new Exception("Wrong ID");
            }
            else
            {
                string command = $"SELECT * FROM [Towns] WHERE [TownId]={id}";
                SqlCommand cmd = new SqlCommand(command);
                SqlConnection connection = new SqlConnection(connectionString);
                connection.Open();
                Towns elem = null;
                cmd.Connection = connection;
                SqlDataReader dbreader = cmd.ExecuteReader();
                if (dbreader.Read())
                {
                    elem = new Towns(dbreader.GetInt32(0), dbreader.GetString(1));
                }
                connection.Close();
                return elem;
            }
        }

        public void Update(Towns obj)
        {
            string command = $"UPDATE [Towns] SET Name=@Name WHERE TownId=@Id";
            SqlCommand cmd = new SqlCommand(command);
            cmd.Parameters.AddWithValue("@Id", obj.Id);
            cmd.Parameters.AddWithValue("@Name", obj.Name);
            SqlConnection connection = new SqlConnection(connectionString);
            connection.Open();
            cmd.Connection = connection;
            cmd.ExecuteNonQuery();
            connection.Close();
        }
    }
}
