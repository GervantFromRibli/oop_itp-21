﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Lab2DOM;
using Trains;
using System.Collections.Generic;

namespace ReadTests
{
    [TestClass]
    public class ReadAndWriteTests
    {
        [TestMethod]
        public void FirstTest()
        {
            DOMTree.path = "d:\\Test.xml";
            List<Train> list = new List<Train>();
            Train train = new Train(545, "wadwdada", "dwadae", 15, 12, 45, 23, Types.Пассажирский);
            list.Add(train);
            DOMTree.CreateDoc(list);
            list = DOMTree.ReadDoc();
            DOMTree.path = "d:\\Trains.xml";
            Assert.AreEqual(Types.Пассажирский, list[0].type);
        }
        [TestMethod]
        public void SecondTest()
        {
            DOMTree.path = "d:\\Test.xml";
            List<Train> list = new List<Train>();
            Train train = new Train(545, "wadwdada", "dwadae", 15, 12, 45, 23, Types.Пассажирский);
            list.Add(train);
            train = new Train(400, "sdawda", "dwa112dae", 13, 12, 45, 50, Types.Скорый);
            list.Add(train);
            train = new Train(450, "qdwqqa", "dwadwaadwdae", 15, 12, 45, 23, Types.Экспресс);
            list.Add(train);
            DOMTree.CreateDoc(list);
            list = DOMTree.ReadDoc();
            DOMTree.path = "d:\\Trains.xml";
            Assert.AreEqual(Types.Скорый, list[1].type);
        }
        [ExpectedException(typeof(Exception))]
        [TestMethod]
        public void ThirdTest()
        {
            Train train = new Train(54555, "wadwdada", "dwadae", 15, 12, 45, 23, Types.Пассажирский);
        }
        [ExpectedException(typeof(Exception))]
        [TestMethod]
        public void FouthTest()
        {
            Train train = new Train(545, "wadwdada", "dwadae", 60, 12, 45, 23, Types.Пассажирский);
        }
    }
}
