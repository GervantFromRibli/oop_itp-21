﻿using DAL;
using DBElements;
using System;
using System.Drawing;
using System.Windows.Forms;
using Commands;

namespace TrainApp
{
    public partial class Form1 : Form
    {
        ListBox info = new ListBox()
        {
            Location = new Point(50, 50),
            Size = new Size(900, 250),
            MultiColumn = true
        };
        Label type_of_data = new Label()
        {
            Location = new Point(300, 310),
            Text = "Тип данных"
        };
        ComboBox data_types = new ComboBox()
        {
            Location = new Point(410, 310)
        };
        Label numb = new Label()
        {
            Location = new Point(50, 350),
            Text = "Номер",
            Size = new Size(50, 20)
        };
        TextBox id = new TextBox()
        {
            Location = new Point(100, 350),
            Size = new Size(70, 20)
        };
        Label depart_label = new Label()
        {
            Location = new Point(185, 350),
            Text = "Место отправления",
            Size = new Size(120, 20)
        };
        ComboBox departPlace = new ComboBox()
        {
            Location = new Point(310, 350)
        };
        TextBox name = new TextBox()
        {
            Location = new Point(310, 350),
            Size = new Size(120,20)
        };
        Label arrive = new Label()
        {
            Location = new Point(440, 350),
            Text = "Место прибытия"
        };
        ComboBox arrivePlace = new ComboBox()
        {
            Location = new Point(560, 350)
        };
        Label depTime = new Label()
        {
            Location = new Point(60, 400),
            Text = "Время отправления",
            Size = new Size(120, 20)
        };
        TextBox timeDep = new TextBox()
        {
            Location = new Point(190, 400),
            Size = new Size(70, 20)
        };
        Label pathTime = new Label()
        {
            Location = new Point(270, 400),
            Text = "Время в пути",
            Size = new Size(100, 20)
        };
        TextBox timePath = new TextBox()
        {
            Location = new Point(370, 400),
            Size = new Size(70, 20)
        };
        Label typeLab = new Label()
        {
            Location = new Point(450, 400),
            Text = "Тип",
            Size = new Size(50, 20)
        };
        ComboBox typeOfTrain = new ComboBox()
        {
            Location = new Point(510, 400),
            Size = new Size(80, 20)
        };
        Button add = new Button()
        {
            Location = new Point(60, 460),
            Text = "Добавить"
        };
        Button update = new Button()
        {
            Location = new Point(160, 460),
            Text = "Обновить"
        };
        Button delete = new Button()
        {
            Location = new Point(260, 460),
            Text = "Удалить"
        };
        TownDAO TownDAO = new TownDAO();
        TypesDAO TypesDAO = new TypesDAO();
        TrainDAO TrainDAO = new TrainDAO();
        public Form1()
        {
            InitializeComponent();
            Width = 1000;
            Height = 600;
            Controls.Add(info);
            Controls.Add(type_of_data);
            Controls.Add(data_types);
            data_types.Items.AddRange(new string[] { "Поезд", "Город" });
            data_types.Text = "Поезд";
            GetInfo();
            data_types.SelectedValueChanged += ChangeFields;
            Controls.AddRange(new Control[] { numb, id, depart_label, departPlace, arrive, arrivePlace, timeDep, depTime, timePath, pathTime, typeLab, typeOfTrain, add, update, delete });
            departPlace.Items.AddRange(Comm.GetNames(TownDAO.GetAll()));
            arrivePlace.Items.AddRange(Comm.GetNames(TownDAO.GetAll()));
            typeOfTrain.Items.AddRange(Comm.GetNames(TypesDAO.GetAll()));
            add.Click += AddData;
            update.Click += UpdateData;
            delete.Click += DeleteData;
        }
        private void ChangeFields(object sender, EventArgs e)
        {
            if (data_types.SelectedItem.ToString() == "Поезд")
            {
                Controls.Remove(name);
                numb.Text = "Номер";
                Controls.AddRange(new Control[] { departPlace, arrivePlace, arrive, depTime, timeDep, pathTime, timePath, typeLab, typeOfTrain });
                depart_label.Text = "Место отправления";
                departPlace.Items.Clear();
                departPlace.Items.AddRange(Comm.GetNames(TownDAO.GetAll()));
                arrivePlace.Items.Clear();
                arrivePlace.Items.AddRange(Comm.GetNames(TownDAO.GetAll()));
                typeOfTrain.Items.Clear();
                typeOfTrain.Items.AddRange(Comm.GetNames(TypesDAO.GetAll()));
            }
            else
            {
                Controls.Remove(departPlace);
                Controls.Remove(arrivePlace);
                Controls.Remove(arrive);
                Controls.Remove(depTime);
                Controls.Remove(timeDep);
                Controls.Remove(pathTime);
                Controls.Remove(timePath);
                Controls.Remove(typeLab);
                Controls.Remove(typeOfTrain);
                Controls.Add(name);
                numb.Text = "id";
                depart_label.Text = "Название";
            }
            GetInfo();
        }
        private void AddData(object sender, EventArgs e)
        {
            if (data_types.SelectedItem.ToString() == "Поезд" || data_types.Text == "Поезд")
            {
                if (id.Text.Length == 3 && departPlace.SelectedItem != null && arrivePlace.SelectedItem != null && timeDep.Text.Trim().Split(':').Length == 2 && timePath.Text.Trim().Split(':').Length == 2 && typeOfTrain.SelectedItem != null && TrainDAO.Read(Convert.ToInt32(id.Text.ToString())) == null)
                {
                    TrainDAO.Create(new Train(Convert.ToInt32(id.Text.ToString()), departPlace.SelectedItem.ToString(), arrivePlace.SelectedItem.ToString(), Comm.GetTime(timeDep.Text), Comm.GetTime(timePath.Text), typeOfTrain.SelectedItem.ToString()));
                }
            }
            else
            {
                if (id.Text.Length == 2 && name.Text.Length != 0 && TownDAO.Read(Convert.ToInt32(id.Text.ToString())) == null)
                {
                    TownDAO.Create(new Towns(Convert.ToInt32(id.Text.ToString()), name.Text.ToString()));
                }
            }
            GetInfo();
        }
        private void GetInfo()
        {
            info.Items.Clear();
            if (data_types.SelectedItem.ToString() == "Поезд" || data_types.Text == "Поезд")
            {
                foreach (var elem in TrainDAO.GetAll())
                {
                    info.Items.Add(elem.Id.ToString() + "; " + elem.DepartPlace + "; " + elem.ArrivePlace + "; " + elem.DepartTime.ToString() + "; " + elem.PathTime.ToString() + "; " + elem.type + Environment.NewLine);
                }
            }
            else
            {
                foreach (var elem in TownDAO.GetAll())
                {
                    info.Items.Add(elem.Id.ToString() + "; " + elem.Name + Environment.NewLine);
                }
            }
        }
        private void UpdateData(object sender, EventArgs e)
        {
            if (data_types.SelectedItem.ToString() == "Поезд" || data_types.Text == "Поезд")
            {
                if (id.Text.Length == 3 && departPlace.SelectedItem != null && arrivePlace.SelectedItem != null && timeDep.Text.Trim().Split(':').Length == 2 && timePath.Text.Trim().Split(':').Length == 2 && typeOfTrain.SelectedItem != null)
                {
                    TrainDAO.Update(new Train(Convert.ToInt32(id.Text.ToString()), departPlace.SelectedItem.ToString(), arrivePlace.SelectedItem.ToString(), Comm.GetTime(timeDep.Text), Comm.GetTime(timePath.Text), typeOfTrain.SelectedItem.ToString()));
                }
            }
            else
            {
                if (id.Text.Length == 2 && name.Text.Length != 0)
                {
                    TownDAO.Update(new Towns(Convert.ToInt32(id.Text.ToString()), name.Text.ToString()));
                }
            }
            GetInfo();
        }
        private void DeleteData(object sender, EventArgs e)
        {
            if (data_types.SelectedItem.ToString() == "Поезд" || data_types.Text == "Поезд")
            {
                if (id.Text.Length == 3)
                {
                    TrainDAO.Delete(Convert.ToInt32(id.Text.ToString()));
                }
            }
            else
            {
                if (id.Text.Length == 2)
                {
                    TownDAO.Delete(Convert.ToInt32(id.Text.ToString()));
                }
            }
            GetInfo();
        }
    }
}
