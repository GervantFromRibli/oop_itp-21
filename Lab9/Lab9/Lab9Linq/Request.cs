﻿using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace Lab9Linq
{
    public class Request
    {
        public static IEnumerable<IGrouping<string, string>> TaskRequest()
        {
            var Words = File.ReadAllText("d:\\lab9.txt").Split('\n',' ','\r').Select(word => word.ToLower()).Where(word => word.Length > 4).GroupBy(word => word).OrderByDescending(word => word.Count()).Take(10);
            return Words;
        }
    }
}
